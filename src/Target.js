import React, { Component } from "react";
import { DropTarget } from "react-dnd";

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        hovered: monitor.isOver(),
        item: monitor.getItem(),
    }
}

const itemSource = {
  drop(props, monitor, component) {
    const item = monitor.getItem();
    // console.log(monitor.getDropResult());
    props.onDrop(item);
    // console.log(item);
    return item;
  }
};

class Target extends Component {
    constructor(props) {
        super(props);
        // console.log(props);
        this.state = {
          imagep: []
        };
    }

    // componentWillMount() {
    //     this.setState(prevState => ({
    //         imagep: [...prevState.imagep, this.props.item]
    //     }));
    //     console.log(this.state);
    // }

    // componentWillUpdate() {
    //     this.setState(prevState => ({
    //       imagep: [...prevState.imagep, this.props.item]
    //     }));
    //     console.log(this.state);
    // }

    // componentDidMount() {
    //         this.setState(prevState => ({
    //         imagep: [...prevState.imagep, this.props.item]
    //         }));
    //         console.log(this.state);
    // }

  render() {
      const { connectDropTarget, hovered, components } = this.props;
    //   var ttt;
        // if(item !== null) {
        //     ttt = (
        //       <img
        //         src={item.download_url}
        //         alt={item.id}
        //         style={{ width: "350px" }}
        //       />
        //     );
        // }
    //   console.log(item);
      
      return connectDropTarget(<div className="col-md-10">
        {
            components.map((img, i) => {
                return (
                        <img src={img.download_url} alt={img.id} style={{ width: '380px', padding: '10px' }} />
                );
            })
        }
    </div>);
  }
}

export default DropTarget("item", itemSource, collect)(Target);

