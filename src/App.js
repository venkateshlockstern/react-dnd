import React, { Component } from "react";
import "./App.css";
// import Dragula from "react-dragula";
import axios from "axios";
import HTML5Backend from "react-dnd-html5-backend";
import { DragDropContext } from "react-dnd";
import Item from "./Item";
import Target from "./Target";

class App extends Component {
  constructor(props) {
    super();
    this.state = {
      images: [],
      components: []
    };
    this.onDrop = this.onDrop.bind(this);
  }

  componentWillMount() {
    axios
      .get("https://picsum.photos/v2/list")
      .then(res => {
        this.setState({ images: res.data });
        // console.log(res.data)
      })
      .catch(err => console.log(err));
  }

  deleteItem(id) {
    console.log("Deleting Id: " + id);
    this.setState(prevState => {
      let images = prevState.images;

      const index = images.findIndex(image => image.id === id);

      images.splice(index, 1);

      return { images };
    })
  }


  onDrop(item) {
                 // const { components } = this.state;
                //  this.setState({ components: item });
                //  console.log(this.state.components);
                         this.setState(prevState => ({
                           components: [
                             ...prevState.components,
                             item
                           ]
                         }));
                         console.log(this.state);
                 // const newComponentsList = component;
                 // this.setState({
                 //   images: item
                 // });
               }

  render() {
    // const image = this.state.images.map((img, i) => {
    //   return (
    //     <div className="side-img">
    //       <img src={img.download_url} alt={i} />
    //     </div>
    //   );
    // });
    return (
      <div className="row">
        <div className="col-md-2 sidebar">
          {this.state.images.map((img, i) => {
            return (
              <Item
                key={img.id}
                item={img}
                handleDrop={id => this.deleteItem(id)}
              />
            );
          })}
        </div>

        <Target onDrop={this.onDrop} components={this.state.components}/>
      </div>
    );
  }
}

export default DragDropContext(HTML5Backend)(App);
